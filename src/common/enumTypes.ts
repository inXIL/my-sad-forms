export enum NotifyType {
  idOfCustomers,
  idOfOrders,
  idOfModels,
  listOfCust,
}

export enum SendType {
  customer,
  order,
}
