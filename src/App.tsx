import React, { useState } from 'react';
import CustomerSearch from './components/CustomerSearch';
import Navbar, { NavBarState } from './components/Navbar';
import OrderSearch from './components/OrderSearch';
import Revenue from './components/Revenue';


function App(): JSX.Element {
  const [navBarState, setNavBarState] = useState(NavBarState.OrderSearch);
  let jsx = <></>;
  switch(navBarState) {
    case NavBarState.CustomerSearch: jsx = <CustomerSearch/>; break;
    case NavBarState.OrderSearch: jsx = <OrderSearch/>; break;
    case NavBarState.Revenue: jsx = <Revenue/>; break;
  }
  return (
    <div className="App">
      <Navbar navBarState={navBarState} setNavBarState={setNavBarState}/>
      {jsx}
    </div>
  );
}

export default App;
